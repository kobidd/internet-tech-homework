// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false ,
  firebaseConfig: {
    apiKey: "AIzaSyDKdYR29eGQ16dhMkyGX6yhHfAjm3U9eB8",
    authDomain: "tech-homework.firebaseapp.com",
    databaseURL: "https://tech-homework.firebaseio.com",
    projectId: "tech-homework",
    storageBucket: "tech-homework.appspot.com",
    messagingSenderId: "4860432212",
    appId: "1:4860432212:web:a1ea6c88067860331782c1",
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
