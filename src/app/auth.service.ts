import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user:Observable<User | null>;

  constructor(public afAuth:AngularFireAuth,
              private router:Router) { 
    this.user=this.afAuth.authState;
  }
  private logUser:firebase.User = null
  signup(email:string , password:string){
            this.afAuth
              .auth
              .createUserWithEmailAndPassword(email,password)
              .then(user => {
                this.router.navigate(['/posts'])
              })
              
          }
  logout(){
    this.afAuth.auth.signOut();
  }
login(email:string,password:string){
  this.afAuth
  .auth
  .signInWithEmailAndPassword(email,password)
  .then(user=>{
    this.router.navigate(['/posts'])
  })

}

isLogIn(){
  if(this.logUser == null){
    return false;
  }
  return true;
}

}
