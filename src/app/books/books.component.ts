import { Component, OnInit } from '@angular/core';
import { AuthorsService } from './../authors.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  //books:object[] = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]
  //books:any;
  books$:Observable<any>;
  constructor(private bookservice:AuthorsService) { }

  ngOnInit() {
    this.books$ = this.bookservice.getBooks()
  }

}
