import { AuthService } from './../auth.service';
import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
id:string;
title:string;
body:string;
author:string;
isEdit:boolean = false;
buttonText:string = "Add Post"
userId:string;

  constructor(private postsservice:PostsService, 
               private router:Router,
               private route:ActivatedRoute,
               private authservice:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.authservice.user.subscribe(
      user =>{
        this.userId = user.uid;
     
    if(this.id){
      this.isEdit = true;
      this.buttonText = "Update Post"

      this.postsservice.getPost(this.userId,this.id).subscribe(
        post =>{
          this.title = post.data().title;
          this.body = post.data().body;
          this.author = post.data().author;

        })
      }
    })
  }
  onSubmit(){
    if(this.isEdit){
      this.postsservice.updatePost(this.userId,this.id,this.title,this.body,this.author)
    }
    else{
      this.postsservice.addPost(this.userId,this.title,this.body,this.author)
    }
    this.router.navigate(['/posts']);
  }

}
