import { SignUpComponent } from './sign-up/sign-up.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';



import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BooksComponent } from './books/books.component';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule, Routes } from '@angular/router';
import { EditauthorComponent } from './editauthor/editauthor.component';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import { PostsComponent } from './posts/posts.component';
import { PostsService } from './posts.service';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { PostFormComponent } from './post-form/post-form.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AngularFireAuth } from '@angular/fire/auth';
import { LogInComponent } from './log-in/log-in.component';




const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors', component: AuthorsComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'postsForm', component: PostFormComponent },
  { path: 'postsForm/:id', component: PostFormComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'login', component: LogInComponent },
  { path:"",
    redirectTo: '/books',
    pathMatch: 'full'
  }
  ];

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    NavComponent,
    AuthorsComponent,
    EditauthorComponent,
    PostsComponent,
    PostFormComponent,
    SignUpComponent,
    LogInComponent
  ],
  imports: [
     BrowserModule,
     AngularFireModule.initializeApp(environment.firebaseConfig),
     AngularFireStorageModule, 
    RouterModule.forRoot(
     appRoutes,
     { enableTracing: true } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    MatCardModule,
    FormsModule,
    MatExpansionModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatInputModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    MatFormFieldModule,
  ],
  providers: [PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
