import { AuthService } from './../auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {

  constructor(public authservice:AuthService,
    public router:Router) { }
    email:string;
    password:string;

  onSubmit() {
    this.authservice.login(this.email,this.password);
    
  }
  ngOnInit(){

  }
  isLogIn(){
   return this.authservice.isLogIn();
  }
}
