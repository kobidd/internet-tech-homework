import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(public authservice:AuthService,
              public router:Router) { }

  email:string;
  password:string;

  onSubmit(){
    this.authservice.signup(this.email,this.password);
    this.router.navigate(['/books']);
  }

  ngOnInit() {
  }

login(){
  this.authservice.login(this.email,this.password);
  this.router.navigate(['/books']);
}

logout(){
  this.authservice.logout();
  this.router.navigate(['/login']);
}

isLogIn() {
  this.authservice.isLogIn();
}

}
