import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthorsService {
  authors:any=['Amos oz','J.K.Rolling','Lewis Carrol'];
  getAuthor(){
    const authorsObservable = new Observable(
      observer =>{
        setInterval(
          ()=>observer.next(this.authors),4000
        )
      }
    )
    return authorsObservable;
  }
  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges();
  }
  addAuthor = (name)=> this.authors.push(name);

  constructor(private db:AngularFirestore) { }
}




