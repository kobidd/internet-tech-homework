
import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import { Users } from './interfaces/users';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PostsService {

  private PostsURL = "https://jsonplaceholder.typicode.com/posts/";
  private UsersURL = "https://jsonplaceholder.typicode.com/users/";


  constructor(private http:HttpClient, private db:AngularFirestore) { }
  userCollection: AngularFirestoreCollection  = this.db.collection('users');
  postCollection: AngularFirestoreCollection;
  
  getPosts(userId:string):Observable<any[]>{
    this.postCollection = this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges().pipe(
      map(
        collection => collection.map(
          document=>{
              const data = document.payload.doc.data();
              data.id = document.payload.doc.id;
              return data;
          }
        )
      )
    )
  }
  
  /*getPosts():Observable<any[]>{
    return this.db.collection('posts').valueChanges(({idField:'id' }));
  }*/
  /*getUsers(){
    return this.http.get<Users[]>(this.UsersURL);
  }*/

  addPost(userId:string,title:string,body:string,author:string){
    const post ={title:title,body:body,author:author}
    //this.db.collection('posts').add(post);
    this.userCollection.doc(userId).collection('posts').add(post);
  }

  getPost(userId:string,id:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get()
  }

  updatePost(userId:string,id:string, title:string, body:string, author:string){
    this.db.doc(`users/${userId}/posts/${id}`).update({
      title:title,
      body:body,
      author:author
    })
  }

  deletePost(userId:string,id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  /*private transformPostData(data:PostRaw):Post{
    return{
      userId:data.PostDes[0].userId,
      id: data.PostDes[0].id,
      title: data.PostDes[0].title,
      body:data.PostDes[0].body,
    }*/

  }

