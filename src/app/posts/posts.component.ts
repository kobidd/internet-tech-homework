import { AuthService } from './../auth.service';
import { Observable } from 'rxjs';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
tempData$:Observable<any[]>;
userId:string;
/*user$: Users[]=[];
body:string;
title:string;
author:string;
secAuction:string;*/

  constructor(private postsService:PostsService,
              public authservice:AuthService) { }
  /*saveData(){
    for(let i=0;i<this.tempData$.length;i++){
      for(let j=0;j<this.user$.length;j++){
        if(this.tempData$[i].userId==this.user$[j].id){
          this.body = this.tempData$[i].body;
          this.title = this.tempData$[i].title;
          this.author = this.user$[j].name;
          this.postsService.addPost(this.body,this.title,this.author);
        }
      }
    }
    this.secAuction = "The posts has been saved"
  }*/
  ngOnInit() {
     //this.tempData$ = this.postsService.getPosts();
    //this.postsService.getUsers().subscribe(data => this.user$ = data);
   this.authservice.user.subscribe(
     user =>{
       this.userId = user.uid;
       this.tempData$ = this.postsService.getPosts(this.userId);
     }
   )
  }
  deletePost(id:string){
    this.postsService.deletePost(this.userId,id);
  }

}
